package test.com.maxim.jms.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.maxim.jms.producer.contoller.ProducerController;
import com.maxim.jms.producer.model.Vendor;

public class ProducerControllerTest {

	private Vendor vendor;
	private Model model;
	private ProducerController producerController;
	private ApplicationContext context;

	@Before
	public void setUp() throws Exception {
		
		context=new ClassPathXmlApplicationContext("spring/application-config.xml");
		producerController=(ProducerController) context.getBean("producerController");
		vendor=(Vendor) context.getBean("vendor");
		vendor.setVendorName("UPS");
		vendor.setFirstName("Petar");
		vendor.setLastName("Djurkovic");
		vendor.setAdress("Bulevar 990");
		vendor.setCity("Beograd");
		vendor.setState("Srbija");
		vendor.setPhoneNumber("6743636-236");
		vendor.setZipCode("+381");
		vendor.setEmail("odjf@fkfk.com");
	    
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testRenderVendorPage() {
		assertEquals("index",producerController.renderVendorPage(vendor,model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv=producerController.processRequest(vendor, model);
		assertEquals("index",mv.getViewName());
	}

}
